package com.fary.chapter01_aqs.ext_cyclicBarrier;


import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

public class TestFaryCyclicBarrier {

    public static void main(String[] args) {
        FaryCyclicBarrier cyclicBarrier = new FaryCyclicBarrier(2);

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                int await = cyclicBarrier.await(10, TimeUnit.SECONDS);
                System.out.println(Thread.currentThread().getName());
            }
        }).start();

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                cyclicBarrier.await();
                System.out.println(Thread.currentThread().getName());
            }
        }).start();
    }
}
