package com.fary.chapter04_executors.ext;

public interface FaryExecutor {

    void execute(Runnable command);

}