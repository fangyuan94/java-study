package com.fary.chapter03_ext_callable;

/**
 * @author Fary
 * @version 1.0
 * @description: TODO
 * @date 2021/8/7 16:15
 */
public class TestCallable {
    public static void main(String[] args) throws InterruptedException {
        FaryCallable<String> faryCallable = new FaryCallableImpl();
        FaryFutureTask<String> futureTask = new FaryFutureTask<String>(faryCallable);
        new Thread(futureTask).start();
        String result = futureTask.get();
        System.out.println(Thread.currentThread().getName() + result);
    }
}
