package com.fary.chapter02_safe_thread;

import lombok.SneakyThrows;

/**
 * 使用join让线程按顺序执行
 */
public class SafeThread03 {
    public static void main(String[] args) {
        // t1 run方法执行完毕 唤醒---t2 正在等待
        Thread t1 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                Thread.sleep(1000);
                System.out.println(Thread.currentThread().getName() + ",线程执行");
            }
        }, "t1");
        // t2需要等待t1执行完毕
        Thread t2 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                // t1.wait(); t2调用 t1(this锁).wait() 主动释放this锁 同时t2线程变为阻塞状态。
                t1.join();
                System.out.println(Thread.currentThread().getName() + ",线程执行");
            }
        }, "t2");
        // t3需要等待t2执行
        Thread t3 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                t2.join();
                System.out.println(Thread.currentThread().getName() + ",线程执行");
            }
        }, "t3");
        t1.start();
        t2.start();
        t3.start();
    }
}
