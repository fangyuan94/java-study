package com.fary.chapter01_create_thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 7.使用spring @Async异步注解
 */
@Slf4j
@RestController
public class TestCreateThread04 {

    @Autowired
    private AsyncLogManage asyncLogManage;

    @RequestMapping("/addOrder")
    public String addOrder() {
        log.info("<1>");
        asyncLogManage.asyncLog();
        log.info("<3>");
        return "3";
    }
}


