package com.fary.chapter01_aqs.ext_aqs;

import java.io.Serializable;

public class FaryAbstractOwnableSynchronizer implements Serializable {
    private static final long serialVersionUID = 3737899427754241961L;

    private transient Thread exclusiveOwnerThread;

    protected final void setExclusiveOwnerThread(Thread thread) {
        exclusiveOwnerThread = thread;
    }

    protected final Thread getExclusiveOwnerThread() {
        return exclusiveOwnerThread;
    }
}
