package com.fary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
public class ApplicationThread {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationThread.class, args);
    }

    @Bean
    public ExecutorService getExecutorService() {
        return Executors.newFixedThreadPool(12);
    }
}
