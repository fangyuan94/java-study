package com.fary.chapter01_aqs;

import lombok.SneakyThrows;

import java.util.concurrent.CountDownLatch;

/**
 * @author Fary
 * @version 1.0
 * @description: TestFaryCountDownLatch
 * @date 2021/8/23 13:22
 */
public class TestCountDownLatch {

    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(2);

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                countDownLatch.await();
                System.out.println(Thread.currentThread().getName());
            }
        }).start();

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                countDownLatch.countDown();
                countDownLatch.countDown();
            }
        }).start();
    }
}
