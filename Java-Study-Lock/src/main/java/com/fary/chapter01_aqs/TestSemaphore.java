package com.fary.chapter01_aqs;

import java.util.concurrent.Semaphore;

public class TestSemaphore {
    public static void main(String[] args) throws InterruptedException {

        Semaphore semaphore = new Semaphore(3);

        semaphore.acquire();
        semaphore.acquire();
        semaphore.acquire();
        semaphore.release();
        System.out.println(semaphore.availablePermits());

    }

}
