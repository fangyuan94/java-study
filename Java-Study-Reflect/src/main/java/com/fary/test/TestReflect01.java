package com.fary.test;

import com.fary.po.UserEntity;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @author Fary
 * @version 1.0
 * @description: 获取Class对象的三种方式
 * @date 2021/6/6 18:15
 */
public class TestReflect01 {

    public static void main(String[] args) throws Exception {
        // 1.第一种方式 通过对象.getClass()获取
        Class<?> userClass1 = new UserEntity().getClass();
        // 2.第二种方式 通过类直接获取class
        Class<?> userClass2 = UserEntity.class;
        // 3.第三种方式 类的完整路径地址
        Class<?> userClass3 = Class.forName("com.fary.po.UserEntity");
        System.out.println(userClass3);
        System.out.println(userClass1==userClass2);// true
        System.out.println(userClass1==userClass3);// true

        // 1.通过无参构造函数创建对象
        UserEntity userEntity = (UserEntity) userClass3.newInstance();
        System.out.println(userEntity.toString());

        // 2.通过有参构造函数创建对象
        Constructor<?> constructor = userClass3.getConstructor(String.class, Integer.class);
        UserEntity userEntity2 = (UserEntity) constructor.newInstance("fary", 22);
        System.out.println(userEntity2.toString());

        // 反射执行给私有属性赋值
        Class<?> userClass4 = Class.forName("com.fary.po.UserEntity");
        UserEntity userEntity4 = (UserEntity) userClass4.newInstance();
        Field userName = userClass4.getDeclaredField("name");
        // 设置允许访问私有属性
        userName.setAccessible(true);
        userName.set(userEntity4, "fary");
        System.out.println(userEntity4.getName());

        // 1.反射调用公有方法
        Class<?> userClass5 = Class.forName("com.fary.po.UserEntity");
        UserEntity userEntity5 = (UserEntity) userClass5.newInstance();
        Method addPublic = userClass5.getDeclaredMethod("addPublic", int.class, int.class);
        Object invoke5 = addPublic.invoke(userEntity5, 1, 1);
        System.out.println(invoke5);// 2


        // 2.反射调用私有方法
        Class<?> userClass6 = Class.forName("com.fary.po.UserEntity");
        UserEntity userEntity6 = (UserEntity) userClass6.newInstance();
        Method addPrivate = userClass6.getDeclaredMethod("addPrivate", int.class, int.class);
        // 设置允许调用私有方法
        addPrivate.setAccessible(true);
        Object invoke6 = addPrivate.invoke(userEntity6, 1, 1);
        System.out.println(invoke6);// 2

        // 泛型用在编译期，编译过后泛型擦除（消失掉），所以是可以通过反射越过泛型检查的
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("mayikt");
//        strings.add(1);
        Class<?> aClass7 = strings.getClass();
        Method add = aClass7.getDeclaredMethod("add", Object.class);
        Object invoke = add.invoke(strings, 1);
        System.out.println(strings.size());
        strings.forEach(System.out::println);
    }
}
