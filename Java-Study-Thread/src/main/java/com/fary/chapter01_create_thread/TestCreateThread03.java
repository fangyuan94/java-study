package com.fary.chapter01_create_thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 6.使用Callable和Future创建线程（有返回值）
 * 底层基于LockSupport
 */
public class TestCreateThread03 implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName() + "正在运行");
        return 1;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        TestCreateThread03 testCreateThread03 = new TestCreateThread03();
        FutureTask<Integer> integerFutureTask = new FutureTask<>(testCreateThread03);
        new Thread(integerFutureTask).start();
        Integer integer = integerFutureTask.get();
        System.out.println(integer);
    }
}
