package com.fary.chapter01_aqs.ext_semaphore;

import lombok.SneakyThrows;

/**
 * @author Fary
 * @version 1.0
 * @description: TestFarySemaphore
 * @date 2021/8/21 10:30
 */
public class TestFarySemaphore {

    public static void main(String[] args) throws InterruptedException {
        FarySemaphore semaphore = new FarySemaphore(3);

        for (int i = 0; i < 5; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "线程" + finalI);
                    semaphore.release();
                }
            }).start();
        }
    }
}
