package com.fary.chapter01_create_thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class AsyncLogManage {
    @Async
    public void asyncLog() {
        try {
            Thread.sleep(3000);
            log.info("<2>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}