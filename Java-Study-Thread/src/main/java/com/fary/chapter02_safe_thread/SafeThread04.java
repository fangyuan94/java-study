package com.fary.chapter02_safe_thread;

/**
 * 1.setDaemon 设置为true 守护线程是依赖于用户线程，用户线程退出了，守护线程也就会退出，典型的守护线程如垃圾回收线程。
 * 2.setDaemon 设置为false 用户线程是独立存在的，不会因为其他用户线程退出而退出。
 */
public class SafeThread04 {

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                }
            }
        }, "我是子线程");
        thread.setDaemon(true);
        System.out.println("我的主线程代码执行完毕..");
    }
}
