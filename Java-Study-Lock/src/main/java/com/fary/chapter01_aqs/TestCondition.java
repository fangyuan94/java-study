package com.fary.chapter01_aqs;

import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Fary
 * @version 1.0
 * @description: 测试Condition
 * @date 2021/8/15 17:06
 */
public class TestCondition {

    public static void main(String[] args) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition condition = reentrantLock.newCondition();

        Thread thread1 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                reentrantLock.lock();
                boolean nanos = condition.await(1, TimeUnit.SECONDS);
                System.out.println(nanos);
                reentrantLock.unlock();
            }
        });
        thread1.start();
        System.out.println(Thread.currentThread().getName());

        Thread thread = new Thread(() -> {
            reentrantLock.lock();
            condition.signal();
            reentrantLock.unlock();
        });
        thread.start();

    }
}
