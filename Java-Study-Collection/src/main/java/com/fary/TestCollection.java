package com.fary;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class TestCollection {

    @org.junit.Test
    public void test1() throws Exception {
        LinkedBlockingDeque<String> strings = new LinkedBlockingDeque<>();
        /*   // 从队列中取出元素 如果队列是为空的话 直接返回null*/
//        String result = strings.poll();
        String result = strings.poll(3, TimeUnit.SECONDS);//   从队列中取出元素 如果队列是为空的话  阻塞等待 3s 内如果队列中有新的元素 则获取该元素  否则返回null
        System.out.println("result:" + result);
    }
}
