package com.fary.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Fary
 * @version 1.0
 * @description: 测试注解
 * @date 2021/6/6 19:24
 */
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Fary {

    String name() default "fangyuan";

    int age() default 0;
}
