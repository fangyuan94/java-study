package com.fary.chapter04_executors.ext;

/**
 * 线程工厂
 */
public interface FaryThreadFactory {

    Thread newThread(Runnable r);
}