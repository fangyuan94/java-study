package com.fary.chapter04_executors.springboot;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author HJH
 * @Description: 线程测试控制器
 * @date 2019/8/1 13:45
 */
@RestController
public class ThreadController {

    @Autowired
    private AsyncService asyncService;

    @GetMapping("/thread")
    public void thread() {
        //调用service层的任务
        asyncService.executeAsync();
    }

}