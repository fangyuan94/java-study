package com.fary.chapter03_ext_callable;


public class FaryCallableImpl implements FaryCallable<String> {

    @Override
    public String call() throws InterruptedException {
        Thread.sleep(1000);
        return "";
    }
}
