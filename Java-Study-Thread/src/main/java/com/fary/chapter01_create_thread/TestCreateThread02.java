package com.fary.chapter01_create_thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 2.实现Runnable接口创建线程
 * 3.使用匿名内部类创建线程
 * 4.使用lambda表达式创建线程
 * 5.使用线程池工具类Executors创建线程
 */
public class TestCreateThread02 implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "正在运行");
    }

    public static void main(String[] args) {
        // 实现Runnable接口创建线程
        TestCreateThread02 testCreateThread02 = new TestCreateThread02();
        new Thread(testCreateThread02).start();

        // 使用匿名内部类创建线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "正在运行");
            }
        }).start();

        // 使用lambda表达式创建线程
        new Thread(() -> System.out.println(Thread.currentThread().getName() + "正在运行")).start();

        // 使用线程池工具类Executors创建线程
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "正在运行");
            }
        });
    }
}
