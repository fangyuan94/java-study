package com.fary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationLock {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationLock.class, args);
    }
}
