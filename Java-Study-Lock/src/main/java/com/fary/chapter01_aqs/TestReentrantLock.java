package com.fary.chapter01_aqs;

import java.util.concurrent.locks.ReentrantLock;

public class TestReentrantLock {

    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();
        reentrantLock.lock();

        new Thread(new Runnable() {
            @Override
            public void run() {
                reentrantLock.lock();
                System.out.println(Thread.currentThread().getName() + "子线程");
            }
        }).start();
        System.out.println(Thread.currentThread().getName() + "主线程");
        reentrantLock.unlock();
        System.out.println();
    }

}
