package com.fary.chapter04_executors.ext;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;

@Slf4j
@RestController
public class TestController {

    @Autowired
    private ExecutorService executorService;

    @RequestMapping("/testController")
    public String testController() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("定长线程池" + Thread.currentThread().getName());
            }
        });
        return "3";
    }
}