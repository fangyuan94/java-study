package com.fary.chapter01_aqs.ext_countDownLatch;

import lombok.SneakyThrows;

public class TestFaryCountDownLatch {

    public static void main(String[] args) {
        FaryCountDownLatch countDownLatch = new FaryCountDownLatch(2);

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                countDownLatch.await();
                System.out.println(Thread.currentThread().getName());
            }
        }).start();

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                countDownLatch.countDown();
                countDownLatch.countDown();
            }
        }).start();

        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                countDownLatch.await();
                System.out.println(Thread.currentThread().getName());
            }
        }).start();
    }
}
