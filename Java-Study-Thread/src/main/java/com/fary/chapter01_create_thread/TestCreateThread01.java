package com.fary.chapter01_create_thread;

/**
 * 1.继承Thread创建线程
 */
public class TestCreateThread01 extends Thread {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "正在运行");
    }

    public static void main(String[] args) {
        TestCreateThread01 testCreateThread01 = new TestCreateThread01();
        testCreateThread01.start();
    }
}
