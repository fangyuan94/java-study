package com.fary.chapter01_aqs.ext_condition;

public interface FaryCondition {

    void await() throws InterruptedException;

    void signal();

    void signalAll();

    long awaitNanos(long nanosTimeout) throws InterruptedException;

}
