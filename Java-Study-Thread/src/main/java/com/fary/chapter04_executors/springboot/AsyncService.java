package com.fary.chapter04_executors.springboot;

/**
 * @author HJH
 * @Description: 线程测试service
 * @date 2019/8/1 13:47
 */
public interface AsyncService {

    void executeAsync();

}