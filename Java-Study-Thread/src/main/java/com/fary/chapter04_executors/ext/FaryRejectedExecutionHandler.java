package com.fary.chapter04_executors.ext;

/**
 * 拒绝策略接口
 */
public interface FaryRejectedExecutionHandler {

    void rejectedExecution(Runnable r, FaryThreadPoolExecutor executor);
}