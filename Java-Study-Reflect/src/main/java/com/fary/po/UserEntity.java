package com.fary.po;

import com.fary.annotation.Fary;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Fary
 * @version 1.0
 * @description: 实体对象
 * @date 2021/6/6 18:19
 */
@Data
@AllArgsConstructor
@Fary
public class UserEntity {

    @Fary
    private String name;
    private Integer age;

    @Fary
    public UserEntity() {
    }

    @Fary
    public int addPublic(int a, int b) {
        return a + b;
    }

    @Fary
    private int addPrivate(int a, int b) {
        return a + b;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
