package com.fary.chapter03_ext_callable;

public interface FaryCallable<V> {
    V call() throws InterruptedException;
}