package com.fary.chapter02_safe_thread;

/**
 * interrupt()终止线程
 */
public class SafeThread07 extends Thread{

    private volatile boolean isStart = true;

    @Override
    public void run() {
        while (isStart) {
            // 如果终止了线程,则停止当前线程
            if (this.isInterrupted()) {
                break;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        SafeThread07 safeThread07 = new SafeThread07();
        safeThread07.start();
        Thread.sleep(1000);
        safeThread07.interrupt();
        // isStart=false
    }
}
