package com.fary.chapter02_safe_thread;

/**
 * 使用sleep方法避免cpu空转 防止cpu占用100%
 */
public class SafeThread06 {
    public static void main(String[] args) {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
