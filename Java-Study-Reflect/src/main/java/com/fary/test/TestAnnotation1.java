package com.fary.test;

import com.fary.annotation.Fary;
import com.fary.po.UserEntity;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author Fary
 * @version 1.0
 * @description: 注解
 * @date 2021/6/6 19:09
 */
public class TestAnnotation1 {
    /**
     * @Override 只能标注在子类覆盖父类的方法上面，有提示的作用
     * @Deprecated 标注在过时的方法或类上面，有提示的作用
     * @SuppressWarnings("unchecked") 标注在编译器认为有问题的类、方法等上面，用来取消编译器的警告提示，警告类型有serial、unchecked、unused、all
     *
     * 元注解
     * 元注解用来在声明新注解时指定新注解的一些特性
     * @Target 指定新注解标注的位置，比如类、字段、方法等，取值有ElementType.Method等
     * @Retention 指定新注解的信息保留到什么时候，取值有RetentionPolicy.RUNTIME等
     * @Inherited 指定新注解标注在父类上时可被子类继承
     *
     * TYPE：类、接口（包括注解类型）和枚举的声明
     * FIELD：字段声明（包括枚举常量）
     * METHOD：方法声明
     * PARAMETER：参数声明
     * CONSTRUCTOR：构造函数声明
     * LOCAL_VARIABLE：本地变量声明
     * ANNOTATION_TYPE：注解类型声明
     * PACKAGE：包声明
     * TYPE_PARAMETER：类型参数声明，JavaSE8引进，可以应用于类的泛型声明之处
     * TYPE_USE：JavaSE8引进，此类型包括类型声明和类型参数声明
     */

    /**
     * directly present："直接修饰"注解是指直接修饰在某个元素上的注解；
     * indirectly present："间接修饰"注解就是指得容器注解的数组中指定的注解；
     * present："直接修饰"注解和父类继承下来的"直接注解"的合集；
     * associated："关联"是"直接修饰"注解、"间接修饰"注解以及父类继承下来的注解的合集；
     *
     *      方法	               directly present    indirectly present    present   associated
     * getAnnotation	 	 	        	                                    √
     * getAnnotations	 	 		                                            √
     * getAnnotationsByType	 	 	                                                       √
     * getDeclaredAnnotation		 	 √	 
     * getDeclaredAnnotations		 	 √	 
     * getDeclaredAnnotationsByType		 √	                    √
     *
     */
    public static void main(String[] args) throws Exception {

        // 1.获取当前类上的注解
        Class<?> aClass = Class.forName("com.fary.po.UserEntity");
        Fary declaredAnnotation = aClass.getDeclaredAnnotation(Fary.class);
        Fary annotation = aClass.getAnnotation(Fary.class);
        System.out.println(declaredAnnotation);
        System.out.println(annotation);

        // 2.获取当前方法上的注解
        Method addPublic = aClass.getDeclaredMethod("addPublic", int.class, int.class);
        Fary declaredAnnotation2 = addPublic.getDeclaredAnnotation(Fary.class);
        System.out.println(declaredAnnotation2);

        // 3.获取字段上的注解
        Field pubUserName = aClass.getDeclaredField("name");
        Fary declaredAnnotation3 = pubUserName.getDeclaredAnnotation(Fary.class);
        System.out.println(declaredAnnotation3);

        // 4.获得构造方法注解
        Constructor<?> constructors = aClass.getConstructor();// 先获得构造方法对象
        Fary constructorAnnotation = constructors.getAnnotation(Fary.class);// 拿到构造方法上面的注解实例
        System.out.println(constructorAnnotation.name() + "+" + constructorAnnotation.age());
    }
}
