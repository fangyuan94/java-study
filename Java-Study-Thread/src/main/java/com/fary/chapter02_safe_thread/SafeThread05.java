package com.fary.chapter02_safe_thread;

/**
 * yied放弃CPU执行权
 */
public class SafeThread05 extends Thread {

    public SafeThread05(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            if (i == 30) {
                System.out.println(Thread.currentThread().getName() + ",释放cpu执行权");
                Thread.yield();
            }
            System.out.println(Thread.currentThread().getName() + "," + i);
        }
    }

    public static void main(String[] args) {
        new SafeThread05("fary01").start();
        new SafeThread05("fary02").start();
    }
}
