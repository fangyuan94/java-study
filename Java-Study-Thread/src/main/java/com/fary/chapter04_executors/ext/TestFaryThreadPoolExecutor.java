package com.fary.chapter04_executors.ext;

import lombok.SneakyThrows;
import org.junit.Test;

import java.util.concurrent.*;

public class TestFaryThreadPoolExecutor {

    public static void main(String[] args) {
        FaryExecutorService faryExecutorService = FaryExecutors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            faryExecutorService.execute(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread() + "Fary手写线程池");
                }
            });
        }
    }

}
